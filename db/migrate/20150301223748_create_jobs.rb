class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name
      t.string :location
      t.date :due_date
      t.boolean :complete

      t.timestamps null: false
    end
  end
end
