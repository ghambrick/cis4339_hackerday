json.array!(@jobs) do |job|
  json.extract! job, :id, :name, :location, :due_date, :complete
  json.url job_url(job, format: :json)
end
