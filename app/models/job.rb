class Job < ActiveRecord::Base

  has_many :tasks
  validates :name, :due_date, presence: true
  validate :valid_date

  def valid_date
  if due_date.present? && due_date <=Date.today
    errors.add(:due_date, "can't be today or in the past")

  end
  end
end
